This project uses computer graphics technology and combines C++ with OpenGL to develop software that can generate Celtic Knots. Please introduce the glut library and configure the OpenGL environment on VS 2017 before running.


**Build and Run**


This project is developed on the following specifications:
- OS: Windows 64-bit
- CPU: Intel(R) Core(TM) i7-9750H CPU @ 2.60GHz 2.59 GHz
- RAM: 8.00 GB

Compiler: Visual Studio Community 2017 only for Windows 
https://my.visualstudio.com/Downloads?q=visual%20studio%202017

Glut library for Windows 
https://www.opengl.org/resources/libraries/glut/glutdlls37beta.zip

1. Download glut library and extract 
2. Find the VS2017 installation directory,put glut.h into... \VC\Tools\MSVC\14.10.25017\include\gl (if you don't have a gl folder, create a new one) 
3. Put glut.lib,glut32.lib into... \VC\Tools\MSVC\14.10.25017\lib\x86 
4. Put glut.dll,glut32.dll in C:\Windows\System64 or C:\Windows\System32 
5. Open VS2017, create a new win32 console project, select empty project 
6. Click on Projects above and select Manage NuGet packages. 
7. Search for nupengl and install both
