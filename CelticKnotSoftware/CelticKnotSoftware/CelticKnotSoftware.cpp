﻿#include "GL/glut.h"
#include "GL/gl.h"
#include <iostream>
#include <vector>
#include <windows.h>

using namespace std;

// type of point with horizontal and vertical coordinates
struct Point {
	double x, y;
};

// type of points set
typedef vector<Point> Points;

// set canvas parameters
#define WIDTH 4 // column of of celtic knot partten
#define HEIGHT 3 // row of celtic knot partten

// draw polygons by 2D point coordinates with OpenGL
// pts - coordinates of the vertices of polygons
void drawPoints(Points& pts) {
	glBegin(GL_POLYGON); // draw filled convex polygons
	for (unsigned int i = 0; i < pts.size(); i++)
		glVertex2d(pts[i].x, pts[i].y);
	glEnd();
}

// drawing quadratic Bezier Curve
// start - coordinates of the starting point of the curve
// end - coordinates of the ending point of the curve
// mid - coordinates of the control point
void drawBezierCurvePoints(Point start, Point end, Point control, Points& pts) {
	for (double t = 0; t <= 1; t += 0.0001) {
		double bx = (1 - t) * (1 - t) * start.x + 2 * t * (1 - t) * control.x + t * t * end.x;
		double by = (1 - t) * (1 - t) * start.y + 2 * t * (1 - t) * control.y + t * t * end.y;
		Point bp = { bx, by };
		pts.push_back(bp); // put Bezier Curve points into point set pts
	}
}

// drawing the upper border of the Celtic Knot 
void drawUpperCurve(double x, double y, double dx, double dy) {
	// 0.4 - relative width of Celtic knot thread
	// x - horizontal coordinate of the centre point of the diamong
	// y - vertical coordinate of the centre point of the diamong
	// dx - half the length of the horizontal diagonal of the diamond
	// dy - half the length of the vertical diagonal of the diamond
	float  curvature = 0.3; // relative curvature of curves
	struct Point topl = { x - dx / 2 - dx * 0.4 / 2, y - dy / 2 + dy * 0.4 / 2 };
	struct Point botl = { x - dx / 2 + dx * 0.4 / 2, y - dy / 2 - dy * 0.4 / 2 };
	struct Point topr = { x + dx / 2 + dx * 0.4 / 2, y - dy / 2 + dy * 0.4 / 2 };
	struct Point botr = { x + dx / 2 - dx * 0.4 / 2, y - dy / 2 - dy * 0.4 / 2 };
	struct Point topm = { x, y - dy / 2 + dy * 0.4 / 2 + dy / 2 * curvature * 3 };
	struct Point botm = { x, y - dy / 2 - dy * 0.4 / 2 + dy / 2 * curvature };
	Points pts, points;

	// draw upper edge of Celtic Knot
	// insert all Bezier Curve points into point set
	drawBezierCurvePoints(topl, topr, topm, pts);
	points.insert(points.end(), pts.begin(), pts.end());
	drawBezierCurvePoints(botr, botl, botm, pts);
	points.insert(points.end(), pts.begin(), pts.end());
	drawPoints(points);
}

// drawing the bottom border of the Celtic Knot 
void drawBottomCurve(double x, double y, double dx, double dy) {
	// 0.4 - relative width of Celtic knot thread
	// x - horizontal coordinate of the centre point of the diamong
	// y - vertical coordinate of the centre point of the diamong
	// dx - half the length of the horizontal diagonal of the diamond
	// dy - half the length of the vertical diagonal of the diamond
	float  curvature = 0.3; // relative curvature of curves
	struct Point topl = { x - dx / 2 + dx * 0.4 / 2, y + dy / 2 + dy * 0.4 / 2 };
	struct Point botl = { x - dx / 2 - dx * 0.4 / 2, y + dy / 2 - dy * 0.4 / 2 };
	struct Point topr = { x + dx / 2 - dx * 0.4 / 2, y + dy / 2 + dy * 0.4 / 2 };
	struct Point botr = { x + dx / 2 + dx * 0.4 / 2, y + dy / 2 - dy * 0.4 / 2 };
	struct Point topm = { x, y + dy / 2 + dy * 0.4 / 2 - dy / 2 * curvature };
	struct Point botm = { x, y + dy / 2 - dy * 0.4 / 2 - dy / 2 * curvature * 3 };
	Points pts, points;

	// draw upper edge of Celtic Knot
	// insert all Bezier Curve points into point set
	drawBezierCurvePoints(botl, botr, botm, pts);
	points.insert(points.end(), pts.begin(), pts.end());
	drawBezierCurvePoints(topr, topl, topm, pts);
	points.insert(points.end(), pts.begin(), pts.end());
	drawPoints(points);
}

// drawing the left border of the Celtic Knot 
void drawLeftCurve(double x, double y, double dx, double dy) {
	// 0.4 - relative width of Celtic knot thread
	// x - horizontal coordinate of the centre point of the diamong
	// y - vertical coordinate of the centre point of the diamong
	// dx - half the length of the horizontal diagonal of the diamond
	// dy - half the length of the vertical diagonal of the diamond
	float  curvature = 0.3; // relative curvature of curves
	struct Point topl = { x + dx / 2 - dx * 0.4 / 2, y + dy / 2 + dy * 0.4 / 2 };
	struct Point botl = { x + dx / 2 - dx * 0.4 / 2, y - dy / 2 - dy * 0.4 / 2 };
	struct Point topr = { x + dx / 2 + dx * 0.4 / 2, y + dy / 2 - dy * 0.4 / 2 };
	struct Point botr = { x + dx / 2 + dx * 0.4 / 2, y - dy / 2 + dy * 0.4 / 2 };
	struct Point leftm = { x + dx / 2 - dx * 0.4 / 2 - dx / 2 * curvature * 3, y };
	struct Point rightm = { x + dx / 2 + dx * 0.4 / 2 - dx / 2 * curvature, y };
	Points pts, points;

	// draw upper edge of Celtic Knot
	// insert all Bezier Curve points into point set
	drawBezierCurvePoints(topl, botl, leftm, pts);
	points.insert(points.end(), pts.begin(), pts.end());
	drawBezierCurvePoints(botr, topr, rightm, pts);
	points.insert(points.end(), pts.begin(), pts.end());
	drawPoints(points);
}

// drawing the right border of the Celtic Knot 
void drawRightCurve(double x, double y, double dx, double dy) {
	// 0.4 - relative width of Celtic knot thread
	// x - horizontal coordinate of the centre point of the diamong
	// y - vertical coordinate of the centre point of the diamong
	// dx - half the length of the horizontal diagonal of the diamond
	// dy - half the length of the vertical diagonal of the diamond
	float  curvature = 0.3; // relative curvature of curves
	struct Point topl = { x - dx / 2 - dx * 0.4 / 2, y + dy / 2 - dy * 0.4 / 2 };
	struct Point botl = { x - dx / 2 - dx * 0.4 / 2, y - dy / 2 + dy * 0.4 / 2 };
	struct Point topr = { x - dx / 2 + dx * 0.4 / 2, y + dy / 2 + dy * 0.4 / 2 };
	struct Point botr = { x - dx / 2 + dx * 0.4 / 2, y - dy / 2 - dy * 0.4 / 2 };
	struct Point leftm = { x - dx / 2 - dx * 0.4 / 2 + dx / 2 * curvature, y };
	struct Point rightm = { x - dx / 2 + dx * 0.4 / 2 + dx / 2 * curvature * 3, y };
	Points pts, points;

	// draw upper edge of Celtic Knot
	// insert all Bezier Curve points into point set
	drawBezierCurvePoints(topr, botr, rightm, pts);
	points.insert(points.end(), pts.begin(), pts.end());
	drawBezierCurvePoints(botl, topl, leftm, pts);
	points.insert(points.end(), pts.begin(), pts.end());
	drawPoints(points);
}

// drawing the Celtic Knot line, top left to bottom right
void drawTLtoBRLine(double x, double y, double dx, double dy) {
	// 0.4 - relative width of Celtic knot thread
	// x - horizontal coordinate of the centre point of the diamong
	// y - vertical coordinate of the centre point of the diamong
	// dx - half the length of the horizontal diagonal of the diamond
	// dy - half the length of the vertical diagonal of the diamond
	struct Point topl = { x - dx / 2 - 0.4 / 2 / WIDTH, y + dy / 2 - 0.4 / 2 / HEIGHT };
	struct Point topr = { x - dx / 2 + 0.4 / 2 / WIDTH, y + dy / 2 + 0.4 / 2 / HEIGHT };
	struct Point botl = { x + dx / 2 - 0.4 / 2 / WIDTH, y - dy / 2 - 0.4 / 2 / HEIGHT };
	struct Point botr = { x + dx / 2 + 0.4 / 2 / WIDTH, y - dy / 2 + 0.4 / 2 / HEIGHT };
	Points pts, points;

	// connect the 4 vertices in order
	points.push_back(topl);
	points.push_back(topr);
	points.push_back(botr);
	points.push_back(botl);
	drawPoints(points);
}

// drawing the Celtic Knot line, top right to bottom left
void drawTRtoBLLine(double x, double y, double dx, double dy) {
	// 0.4 - relative width of Celtic knot thread
	// x - horizontal coordinate of the centre point of the diamong
	// y - vertical coordinate of the centre point of the diamong
	// dx - half the length of the horizontal diagonal of the diamond
	// dy - half the length of the vertical diagonal of the diamond
	struct Point topl = { x + dx / 2 - 0.4 / 2 / WIDTH, y + dy / 2 + 0.4 / 2 / HEIGHT };
	struct Point topr = { x + dx / 2 + 0.4 / 2 / WIDTH, y + dy / 2 - 0.4 / 2 / HEIGHT };
	struct Point botl = { x - dx / 2 - 0.4 / 2 / WIDTH, y - dy / 2 + 0.4 / 2 / HEIGHT };
	struct Point botr = { x - dx / 2 + 0.4 / 2 / WIDTH, y - dy / 2 - 0.4 / 2 / HEIGHT };
	Points pts, points;

	// connect the 4 vertices in order
	points.push_back(topl);
	points.push_back(topr);
	points.push_back(botr);
	points.push_back(botl);
	drawPoints(points);
}

void display() {
	// OpenGL parameters settings
	glClearColor(0.0, 0.0, 0.0, 0.0);
	//glColor4ub(255.0, 255.0, 255.0, 0.0); // default colour
	glColor4ub(254.0, 165.0, 0.0, 0.0); // set drawing colour
	glClear(GL_COLOR_BUFFER_BIT);
	glPointSize(5);
	glLineWidth(100);

	// half the length of the diagonal of a diamond divided by the Celtic knot
	double dx = 1.0 / WIDTH, dy = 1.0 / HEIGHT;

	for (int i = 0; i <= HEIGHT * 2; i += 1) {
		double y = dy * i - 1.0; // current dimonds centre horizontal coordinate
		for (int j = 1 - (i % 2); j <= WIDTH * 2; j += 2) {
			double x = dx * j - 1.0; // current dimonds centre vertical coordinate	
			if (i == 0) { // draw bottom edge curve
				drawBottomCurve(x, y, dx, dy);
			}
			else if (i == HEIGHT * 2) { // draw upper edge curve
				drawUpperCurve(x, y, dx, dy);
			}
			else if (j == 0) { // draw left edge curve
				drawLeftCurve(x, y, dx, dy);
			}
			else if (j == WIDTH * 2) { // draw right edge curve
				drawRightCurve(x, y, dx, dy);
			}
			else if (i % 2 == 1) { // drawing top left to bottom right line when even rows
				drawTLtoBRLine(x, y, dx, dy);
				// drawing the counter top right to bottom left line by the top left to bottom right line
				// 0.1 - relative gap width between two Celtic knot line
				// 0.4 - relative width of Celtic knot line
				// x1, x2 - horizontal coordinate of the centre point of the small rectangle
				// y1, y2 - vertical coordinate of the centre point of the small rectangle
				// dx - half the length of the horizontal diagonal of the diamond which wrap the small rectangle 
				// dy - half the length of the vertical diagonal of the diamond which warp the small rectangle 
				double dx1 = dx * (1 - 0.4 - 0.1) / 2;
				double dy1 = dy * (1 - 0.4 - 0.1) / 2;
				double x1 = x - dx / 2 + dx1 / 2;
				double x2 = x + dx / 2 - dx1 / 2;
				double y1 = y - dy / 2 + dy1 / 2;
				double y2 = y + dy / 2 - dy1 / 2;
				// drawing two small rectangles to present the overlapping line 
				drawTRtoBLLine(x1, y1, dx1, dy1);
				drawTRtoBLLine(x2, y2, dx1, dy1);
			}
			else { // drawing top right to bottom left line when odd rows
				drawTRtoBLLine(x, y, dx, dy);
				// drawing the top left to bottom right line divided by the top right to bottom left line
				double dx1 = dx * (1 - 0.4 - 0.1) / 2;
				double dy1 = dy * (1 - 0.4 - 0.1) / 2;
				double x1 = x - dx / 2 + dx1 / 2;
				double x2 = x + dx / 2 - dx1 / 2;
				double y1 = y + dy / 2 - dy1 / 2;
				double y2 = y - dy / 2 + dy1 / 2;
				drawTLtoBRLine(x1, y1, dx1, dy1);
				drawTLtoBRLine(x2, y2, dx1, dy1);
			}
		}
	}
	glFlush();
}

int main(int argc, char* argv[]) {	
	DWORD time1, time2;
	time1 = GetTickCount();
	int width = 700 / WIDTH * WIDTH;
	int height = 700 / WIDTH * HEIGHT;
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_RGB | GLUT_SINGLE); // use single buffer
	glutInitWindowPosition(100, 100);
	glutInitWindowSize(width, height);
	glutCreateWindow("Celtic Knot"); // title of window	
	glutDisplayFunc(&display);
	time2 = GetTickCount();
	cout << "The running time is " << time2 - time1 << " ms" << endl; // counting running time
	glutMainLoop(); 
	return 0;
}
